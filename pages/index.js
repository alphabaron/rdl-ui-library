// This page has defined `getInitialProps` to do data fetching.
// Next.js will execute `getInitialProps`
// It will wait for the result of `getInitialProps`
// When the results comes back Next.js will render the page.
// Next.js wil do this for every request that comes in.
import fetch from 'isomorphic-unfetch'
import Head from 'next/head'

import { Alert, DropDown, ProgressBar, ProgressCircle, LoadingSpin, LoadingGrow } from '../components/business-facing';

function HomePage() {

    var alertButtonAction = () => {
        console.log('add alert button action...');
    }

    return (
        <>
            <Head>
                <title>RDL UI LIBRARY</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                {/* <!-- Bootstrap CSS --> */}
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossOrigin="anonymous" />
            </Head>

            <Alert alertText="The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph." closeButton icon="info" alertHeader="Primary Alert!" alertButton={{ buttonText: "Close", callback: alertButtonAction }} type="primary" />

            <br />

            <DropDown direction="down" extraClasses="mb-1 mt-1 mr-1" text="Dropdown" icon={{ prefix: "fas", iconName: "user" }} content={[
                { id: "1", link: "https://curiashops.com/", text: "Item 1" },
                { id: "2", link: "https://swoovedelivery.com/", text: "Item 2" },
                { id: "3", link: "https://nsuoapp.com/", text: "Item 3" }]} />

            <br /><br />

            <ProgressBar percentage="50" label />

            <br /><br />

            <ProgressCircle strokeWidth="10" sqSize="200" percentage="53" />

            <br /><br />

            <LoadingSpin fillColor="#dce0ff" />

            <LoadingGrow fillColor="#959ABF" />

            {/* <!-- Optional JavaScript --> */}
            {/* <!-- jQuery first, then Popper.js, then Bootstrap JS --> */}
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossOrigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossOrigin="anonymous"></script>
        </>
    )
}



export default HomePage