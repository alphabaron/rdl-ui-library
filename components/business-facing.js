import React, { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export function Alert(props) {

    if ((props.icon === "") || (props.icon === true) || !props.icon) {
        console.log('no icon');
    } else {
        var icon = <i className="alert-icon" style={{ backgroundImage: `url(/img/icon-${props.icon}.svg)` }}></i>;
    }

    if (props.alertHeader) {
        console.log('cannot have icon and header at the same time');
        icon = "";
        var header = <h4 className="mt-0">{props.alertHeader}</h4>;
    }

    return (
        <div className={`alert alert-${props.type}`}>
            {props.closeButton && <button type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</button>}
            {header}
            <div className="row mx-0 mb-3">
                <span> {icon} {props.alertText} </span>
            </div>
            {props.alertButton &&
                <button onClick={props.alertButton.callback} className="mb-1 mt-1 mr-1 btn btn-secondary">
                    {props.alertButton.buttonText}
                </button>
            }
        </div>
    )
}

export function DropDown(props) {

    return (
        <div className={`btn-group drop${props.direction}`}>
            <button type="button" className={`btn btn-primary dropdown-toggle ${props.extraClasses}`} data-toggle="dropdown">
                {props.icon && <FontAwesomeIcon icon={[props.icon.prefix, props.icon.iconName]} />}
                {props.text && <> {props.text}</>}
            </button>
            <div className="dropdown-menu" role="menu">
                {props.content.map(function (item, i) {
                    return <a className="dropdown-item text-1" key={item.id} id={item.id} href={item.link}>{item.text}</a>
                })}
            </div>
        </div >
    )
}

export function ProgressBar(props) {
    return (
        <>
            <div className="progress rounded-pill">
                <div className="progress-bar rounded-pill" style={{ width: `${props.percentage}%` }}>{props.label && <>{props.percentage}%</>}</div>
            </div>
        </>
    )
}

export function ProgressCircle(props) {
    // Size of the enclosing square
    const sqSize = props.sqSize;
    // SVG centers the stroke width on the radius, subtract out so circle fits in square
    const radius = (props.sqSize - props.strokeWidth) / 2;
    // Enclose cicle in a circumscribing square
    const viewBox = `0 0 ${sqSize} ${sqSize}`;
    // Arc length at 100% coverage is the circle circumference
    const dashArray = radius * Math.PI * 2;
    // Scale 100% coverage overlay with the actual percent
    const dashOffset = dashArray - dashArray * props.percentage / 100;

    return (
        <svg
            viewBox={viewBox}>
            <circle
                className="circle-background"
                cx={props.sqSize / 2}
                cy={props.sqSize / 2}
                r={radius}
                strokeWidth={`${props.strokeWidth}px`} />
            <circle
                className="circle-progress"
                cx={props.sqSize / 2}
                cy={props.sqSize / 2}
                r={radius}
                strokeWidth={`${props.strokeWidth}px`}
                // Start progress marker at 12 O'Clock
                transform={`rotate(-90 ${props.sqSize / 2} ${props.sqSize / 2})`}
                style={{
                    strokeDasharray: dashArray,
                    strokeDashoffset: dashOffset
                }} />
            <text
                className="circle-text"
                x="50%"
                y="50%"
                dy=".3em"
                textAnchor="middle">
                {`${props.percentage}%`}
            </text>

        </svg>
    );
}

export function LoadingSpin(props) {
    return (
        <svg version="1.1" id="Layer_1" viewBox="0 0 500 500" style={{ "enable-background": "new 0 0 500 500;" }} >
            <path id="XMLID_15_" style={{ fill: props.fillColor }} d="M490,250c0,132.5-107.5,240-240,240S10,382.5,10,250S117.5,10,250,10v40 C139.5,50,50,139.5,50,250s89.5,200,200,200s200-89.5,200-200H490z">
                <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 250 250" to="360 250 250" dur="0.6s" repeatCount="indefinite" />
            </path>
        </svg>

    )
}

export function LoadingGrow(props) {
    return (
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 500 500" style={{ "enableBackground": "new 0 0 500 500;" }} space="preserve">
            <circle id="XMLID_1_" className="st1" cx="250" cy="250" r="1" />
            <circle id="XMLID_2_" className="st2" cx="250" cy="250" r="1" />
            <circle id="XMLID_3_" className="st3" cx="250" cy="250" r="1" />
            <style jsx>{`
                .st1{
                    fill: ${props.fillColor};
                    animation-name: circleGrow;
                    animation-duration: 1.5s;
                    animation-iteration-count: infinite;
                    transform-origin: 50% 50%;
                }
        
	            .st2{
                    fill: ${props.fillColor};
                    animation-name: circleGrow;
                    animation-duration: 1.5s;
                    animation-iteration-count: infinite;
                    transform-origin: 50% 50%;
                    animation-delay: .5s;
                }

	            .st3{
                    fill: ${props.fillColor};
                    animation-name: circleGrow;
                    animation-duration: 1.5s;
                    animation-iteration-count: infinite;
                    transform-origin: 50% 50%;
                    animation-delay: 1s;
                }
        
	            @keyframes circleGrow {
                    0 % { transform: scale(1); fill: ${props.fillColor} }
                    100% {transform: scale(250); fill: #DCE0FF00 }
                }
            `}
            </style>
        </svg>
    )
}