import React, {useEffect, useState} from 'react'
export function SquareButton(props){

    return(
        <button 
            onClick={props.callback} 
            style={{...props.defaultStyle, backgroundColor: props.backgroundColor || 'purple', border: 'none', color: props.color || 'white', padding: props.padding || '5px 15px', textAlign: props.alignment || 'center', display: 'inline-block', fontSize: props.fontSize || '14px' }}
        >
            {props.buttonText}
        </button>
    )
}

export function RoundedButton(props){

    return(
        <button
            type="button" 
            onClick={props.callback} 
            style={{...props.defaultStyle, backgroundColor: props.backgroundColor || 'purple', border: 'none', color: props.color || 'white', padding: props.padding || '5px 15px', textAlign: props.alignment || 'center', display: 'inline-block', fontSize: props.fontSize || '14px', borderRadius: props.borderRadius || '5px' }}
            disabled={props.disabled}
        >
            {props.buttonText}
        </button>
    )
}

export function Card(props){

    return(
        <div className="rdl-card" 
            style={{...props.defaultStyle, backgroundColor: props.backgroundColor || "#FFFFFF", border: props.border || '1px solid #DCE0FF', borderRadius: props.borderRadius ||  '5px', opacity: '1', padding: '10px' }}
        >
            <div className="rdl-card-body" style={{padding: '10px'}}>
                {props.children}
            </div>
        </div> 
    )
}

export function CardWithImage(props){

    return(
        <div className="rdl-card" 
            style={{...props.defaultStyle, backgroundColor: props.backgroundColor || "#FFFFFF", border: props.border || '1px solid #DCE0FF', borderRadius: props.borderRadius ||  '5px', opacity: '1' }}
        >   
            <div style={{width: '100%', height: '60%', backgroundImage: props.imageSource, backgroundSize: 'cover'}}></div>
            <div className="rdl-card-body" style={{padding: '10px'}}>
                {props.children}
            </div>
        </div> 
    )
}

export const InputCard = (props) => {

    const inputStyles =  {
        border: '0',
       // borderBottom: `0px solid rgb(249, 123, 77)`,
        outline: '0',
        fontSize: '14px',
        margin: '0px',
        padding: '0px',
        color: props.inputColor || "black",
        backgroundColor: 'transparent',
        width: '100%'
    };

    const [val, setVal] = useState("")
    useEffect(()=>{
        setVal(props.defaultValue)
    }, [props.defaultValue])

    return(
        <div className="card cs-shadow" style={{border: 'none', borderRadius: '0'}}>
            <div className="card-body" style={{paddingTop: '0em', paddingBottom: '0em'}}>
                <label htmlFor={props.inputName} style={{width: '100%', fontSize: props.inputLabelFontSize || '12px'}}>{props.inputLabel} {props.tooltipText && <span style={{float: 'right'}}><i className="fa fa-info-circle"><span className="tooltiptext">{props.tooltipText || "Default tooltip text"}</span></i></span>}</label>
                <input id={props.inputName} disabled={props.disabled} type={props.inputType} style={inputStyles}  value={val} onBlur={(e)=>{props.onFocusOut(props.inputName, e.target.value); console.log("focus out")}} onChange={(e)=>{setVal(e.target.value);props.onValueChange(props.inputName, e.target.value);console.log("changed");}}/>       
            </div>
        </div>      
    )
}


export const InputCardContainer = (props) => {
    
    return(
        <div className="card cs-shadow" style={{border: 'none', borderRadius: '0', height:'10em'}}>
            <div className="card-body" style={{paddingTop: '0em', paddingBottom: '0em'}}>
                <label style={{width: '100%', fontSize:'14px'}} htmlFor="name">Item Properties <span style={{float: 'right'}}><i className="fa fa-info-circle"><span className="tooltiptext">{props.tooltipText || "not set"} </span></i></span></label><br/>

                {props.children}
            </div>
        </div>
    )
}

export const ItemProperties = (props) => {
    const [properties, setProperties] = useState(["isFragile", "isTemperatureSensitive", "isPerishable"])
    const [propKeyVal, setPropertyKeyVal] = useState([{
        "isFragile": false,
        "isPerishable": false,
        "isTemperatureSensitive": false
    }])

    return(
        <div className="card cs-shadow" style={{border: 'none', borderRadius: '0',height:'10em'}}>
            <div className="card-body" style={{paddingTop: '0em', paddingBottom: '0em'}}>
                <label style={{width: '100%', fontSize:'14px'}} htmlFor="name">Item Properties <span style={{float: 'right'}}><i className="fa fa-info-circle"><span className="tooltiptext">{props.tooltipText || "not set"} </span></i></span></label><br/>

                <label className="cs-checkbox-container" style={{fontSize: '14px'}}>
                    <input type="checkbox" name="checkbox"/>Is Fragile
                    <span className="cs-checkbox-checkmark cs-shadow"></span>
                </label> 

                <label className="cs-checkbox-container" style={{fontSize: '14px'}} >
                    <input type="checkbox" name="checkbox"/>Is Perishable
                    <span className="cs-checkbox-checkmark cs-shadow"></span>
                </label> 

                <label className="cs-checkbox-container" style={{fontSize: '14px'}} >
                    <input type="checkbox" name="checkbox"/>Is isTemperature Sensitive
                    <span className="cs-checkbox-checkmark cs-shadow"></span>
                </label> 
            </div>
        </div>      
    )
}


export const ShippingForm = (props) => {


    const dimensions = {
        border: '0',
        borderBottom: props.borderBottomColor || `1px solid rgb(254, 205, 47)`,
        outline: '0',
        fontSize: '17px',
        margin: '0px',
        padding: '0px',
        color: props.textColor || '#EF5A24',
        backgroundColor: props.backgroundColor || 'transparent',
        width: '10%'
    };


    return(
        <div className="card cs-shadow" style={{border: 'none', borderRadius: '0', height:'10em'}}>
            <div className="card-body" style={{paddingTop: '0em', paddingBottom: '0em'}}>
                <label style={{width: '100%', fontSize: '12px'}} htmlFor="name">Weight<span className="" style={{float: 'right'}}><i className="fa fa-info-circle"><span className="tooltiptext">Tooltip</span></i></span></label>
                <input type="text" className="" style={dimensions} /> <br/>

                <label style={{width: '100%', fontSize: '12px'}} htmlFor="name">Dimensions</label>
                <div style={{display: 'flex', alignItems: 'center', flexFlow: 'row wrap', flexWrap: 'wrap',}}>

                    <label htmlFor="name" style={{margin: '5px 10px 5px 0', fontSize: '12px'}}>Length</label>
                    <input type="number" className="" style={dimensions} /> 
                    


                    <label htmlFor="name" style={{margin: '5px 10px 5px 0',fontSize: '12px'}}>Breadth</label>
                    <input type="number" className="" style={dimensions} /> 

                    <label htmlFor="name" style={{margin: '5px 10px 5px 0', fontSize: '12px'}}>Height</label>
                    <input type="number" className="" style={dimensions} /> 
            </div>
            </div>

            
        </div>      
    )
}


export const Table = (props) => {

    return (
        <table className="table" style={{border: 0}}>
            <thead style={{background: '#FFFFFF 0% 0% no-repeat padding-box', boxShadow: '0px 1px 2px #00000029'}}>
                <tr>
                    {props.tableHeaders && props.tableHeaders.map(item => {
                        return(
                            <th key={item} style={{padding: props.thPadding || '0.2em 1em', fontSize: props.thFontSize || '14px'}}>{item}</th>
                        )
                    })
                    }
                </tr>
            </thead>
            <tbody style={{color: '#707070', backgroundColor: '#F0F0F0'}}>
                {props.children}
            </tbody>
        </table>

    )
}



export const CardContainer = props => {
    return (
        <div className="card" style={{padding: '1em', background: '#FFFFFF 0% 0% no-repeat padding-box', boxShadow: '0px 3px 6px #00000029'}}>
            <div className="cardbody">
                {props.children}
            </div>
        </div>
    )
}